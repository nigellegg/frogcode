# README #
Software for running a Frog. 
This has been modified from the software given in http://www.instructables.com/id/Raspberry-PI-and-DHT22-temperature-and-humidity-lo/?ALLSTEPS, which give directions on set-up [their code is for 2 sensors per Pi and includes functions for sending email which are obsolete]. 

The instructables site also gives wiring diagrams.

I am writing these notes assuming you are using Windows (because that is what I m using). You might need to use different packages if installing from an Apple computer. 

1.  The operating system to use is raspbian.  This is a cut down version of Linux.  It includes a number of packages (such as tetris) which we won't need, so we'll remove them later.   
First confirm your micro SSD card is clean: insert the micro SD card in its holder, and insert this into the SD card slot on your PC.  Use SDFormatter (Windows - https://www.sdcard.org/downloads/formatter_4/eula_windows/; Mac - https://www.sdcard.org/downloads/formatter_4/eula_mac/) to ensure that the card is clean. Do not format with the Windows explorer formatter,because this prevents the imager software from accessing the disk.  

2.  Down load rasbian from https://www.raspberrypi.org/downloads/raspbian/ - click on the download zip for Rasbian jessie lite. This gives you (currently) 2016-05-27-rasbian-jessie-lite.zip.  Extract 2016-05-27-raspbiaan-jessie-lite.img.  We are using the lite version as this has fewer programs we don't need (no GUI, no mathematica, no games; this gives us more disk space, and also makes the Pi more secure). 
 
3.  I used Win32DiskImager to install the os on the SD Card.  In the interface, select 2016-05-27-jessie-lite.img under image, and select the correct drive letter.  http://elinux.org/RPi_Easy_SD_Card_Setup has full details of how to do this. 

Your card should now be ready to boot up in your Raspberry Pi.

Remove the card from your computer, and put it into he Raspberry Pi (this is easier to do when the Raspberry Pi is not in a box or case).  There should be a slight click, and the card should stay in.

Connect the Raspberry Pi to screen, keyboard, and power source, and boot up (as there is no GUI, there is no need to have a mouse). 

The first time you boot up, the operating system goes through some configuration; this may take some time.

The default user with raspbian is pi, with password rapsberry.  

Next you need to connect your RPi to the internet.  To find available WiFi networks from the command line, type

    sudo iwlist wlan0 scan

You should see the name of your WiFi network under "ESSID".  To configure the Rpi to use that network, type 

    sudo nano /etc/wpa_supplicant/wpa_supplicant.conf

Add

    network={
        ssid="name_of_your_wifi_network"
        psk="your_wifi_password"
    }

at the bottom of the file, then Ctrl-O to save, Enter, and Ctrl-X to exit.  Reboot (type sudo reboot).  

Before installing any software, it is essential that you update and upgrade the os.  When you have logged in again, type

    sudo apt-get update

followed by 

    sudo apt-get upgrade

After the upgrade command, you will be presented with a list of packages that need to be upgraded, and asked whether you want to proceed; hit 'y' then enter.  These two commands may take quite a while, but are essential in keeping the frog secure. 

We need to install some dependency libraries.  These are packages that components of our script need in order to work.  Type

    sudo apt-get install build-essential python-dev python-openssl

and press enter.  Now install git:

    sudo apt-get install git

This is the package that controls the transfer of code from this site (and others) to your RPi. To install  the Adafruit library (this reds the data coming from the sensor and converts it into readable numbers) type 

    git clone https://github.com/adafruit/Adafruit_Python_DHT.git

To setup this code so it can be used, enter the directory using 

    cd Adafruit_Python_DHT

and type 

    sudo python setup.py install

Now we have the software set up, we can test the sensors; change directory - 

    cd examples

and then type 
  
    sudo python AdafruitDHT.py 22 04

This should give you output looking like

    Temp=22.6* Humidity=38.6%

If you do not get this, check that the jump leads are all connected properly, and the resistor is connected.  Once you have verified that the sensor is working, you can set up the database to hold the data.  To do this, type
 
    sudo apt-get install mysql-server python-mysqldb

The install will stop and prompt you for the root password; I used Frogdb_1 and Frogdb_2 for this.  Whatever you use, save it somewhere as you will need it later.  

Now you need to get into the mysql console to set up the database.  Type

    mysql -u root -p -h localhost

and enter your root password when prompted.  Then create the database called temperatures:

    CREATE DATABASE temperatures;

To create the tables needed put the data in, select the database:

    USE temperatures;

You need to create a user profile that can log into the database from the script, 'logger':

    CREATE USER 'logger'@'localhost' IDENTIFIED BY 'froguser_1';

'froguser_1' is the password for logger; you can (maybe should) use another password, but will have to change it in the script.  Then give rights to use the database to logger:

    GRANT ALL PRIVILEGES ON temperatures.* to 'logger'@'localhost';
    FLUSH PRIVILEGES;

Now root no longer has rights to do anything on this database, and logger does, so we need to exit the console, and log in as logger: 

    quit;
    mysql -u logger -p -h localhost

(give the password you set when you created the user).  Again, select the database:

    USE temperatures;

Create the table temperaturedata:

    CREATE TABLE temperaturedata (dateandtime DATETIME, sensor VARCHAR(32), temperature DOUBLE,
    humidity DOUBLE);

This creates a table structure with a date and time field, a character field for the sensor name, and two fields for temperature and humidity, which will take two decimal places.  Next create the mailsendlog table:

    CREATE TABLE mailsendlog (mailsendtime DATETIME, triggedsensor VARCHAR(32), triggedlimit 
    VARCHAR(10), lasttemperature VARCHAR(10));

(I'm fairly confident that this is not required, but I might have missed something).  You can check that this worked:

    SELECT * FROM temperaturedata;

should give you "Empty Set (0.00 sec)".
and 

    SELECT * FROM mailsendlog;

should give the same.  

Now you need to install the Frog Code:

    git clone https://bitbucket.org/nigellegg/frogcode.git

This will create a frogcode directory on your Pi.  Copy DHT22logger.py and config.json from that directory into the home directory.  

Now edit the config.json file:
    
    sudo nano config.json

In the MYSQL section, change the settings to those you used when setting up the database.  Under SENSORS, give the name for this sensor (I named them Frog1 and Frog2, but you can give them whatever name you want).  Also change gpiosensor1 to 4 (or whichever GPIO pin you have connected to the second pin of the DHT22).

Confirm that AdafruitDHT22.py is in the same directory as DHT22logger.py and config.json. 

Change the permissions on the adafruit script:

    sudo chmod u+x AdafruitDHT.py

Now test the script:

    python DHT22logger.py

The command line prompt should reappear when this is done; the data should hve been saved in the database.  To check that the data has been saved, log in to the database: 

    mysql -u logger -p -h localhost

adding the password you previously set.  Then use:
 
    use temperatures;
    SELECT * FROM temperatures; 

to see the data. 

The final step is to set up the cron job, which will run the script every five minutes (this period was chosen arbitrarily):
Type: 
  
    crontab -e

and add the following line to the file: 

    */5 * * * * python /home/pi/work/DHT22logger.py

To save this, Ctrl+O, Enter, Ctrl+x

The frog is now set up to collect data.

To export the data as csv:

    (SELECT *)
    UNION
    (SELECT *
    FROM temperaturedata
    INTO OUTFILE '/home/pi/work/data.csv'
    FIELDS ENCLOSED BY '"' TERMMINATED BY ';' ESCAPED BY '"'
    LINES TERMINATED BY '\r\n');